#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

#[cfg(not(debug_assertions))]
#[macro_use]
extern crate include_dir;

use serde::Deserialize;
use std::{fmt::Write, io::BufReader, net, thread, time::Duration};

#[cfg(not(debug_assertions))]
mod serve;

type FResult<T = ()> = Result<T, failure::Error>;

fn run() -> FResult {
    #[cfg(not(debug_assertions))]
    let (_sys, _server, server_addr) = {
        let sys = actix::System::new("webview-test");
        let server = serve::build()?;
        let server_addr = server.addrs()[0];
        let server = server.start();
        (sys, server, server_addr)
    };

    #[cfg(debug_assertions)]
    let server_addr = "127.0.0.1:8080/index.html";

    let webview = web_view::builder()
        .title("HCB Monitor")
        .content(web_view::Content::Url(format!("http://{}", server_addr)))
        .size(1400, 700)
        .user_data(())
        .invoke_handler(|_webview, arg| {
            let message = serde_json::from_str::<Message>(arg);

            match message {
                Ok(message) => match message {
                    Message::Print { value } => println!("{}", value),
                },
                Err(_) => eprintln!("Unsupported message: {}", arg),
            }

            Ok(())
        })
        .build()?;

    let handle = webview.handle();
    thread::spawn(move || -> FResult {
        let sock = net::TcpListener::bind("127.0.0.1:50000")?;

        for conn in sock.incoming() {
            println!("New connection");
            let res = (|| -> FResult {
                let conn = conn?;
                conn.set_read_timeout(Some(Duration::from_secs(1)))?;

                handle.dispatch(|webview| webview.eval("clearData();"))?;

                let frames = rlframedump::parse_read(BufReader::new(conn));
                for frame in frames {
                    let frame: rlframedump::Frame = frame?;

                    let mut js_str = "addPoint();var shift = shouldShift();".to_string();

                    for (i, data) in [
                        frame.elapsed,
                        frame.desired_sleep,
                        frame.actual_sleep,
                        frame.actual_sleep - frame.desired_sleep,
                        frame.delta_time,
                        frame.run_frames as f64,
                        frame.leftover_time,
                        frame.total,
                        frame.game,
                        frame.render,
                        frame.gpu,
                    ]
                    .iter()
                    .enumerate()
                    {
                        write!(
                            &mut js_str,
                            "chart.series[{}].addPoint([{}, {}], false, shift, true);",
                            i, frame.index, data
                        )?;
                    }
                    write!(&mut js_str, "drawChart();")?;

                    handle.dispatch(move |webview| webview.eval(&js_str))?;
                }

                Ok(())
            })();
            println!("Connection closed");

            if let Err(ref e) = res {
                if e.downcast_ref::<web_view::Error>().is_some() {
                    res?;
                } else {
                    eprintln!("Error: {}", e);
                }
            }
        }

        Ok(())
    });

    webview.run()?;

    Ok(())
}

#[derive(Debug, Deserialize)]
enum Message<'a> {
    Print { value: &'a str },
}

fn main() -> FResult {
    #[cfg(not(debug_assertions))]
    use std::io::prelude::*;

    if let Err(e) = run() {
        #[cfg(not(debug_assertions))]
        let mut file = std::fs::File::create("hcbmon.log")?;

        eprintln!("\nError:\n\t{}", e);
        #[cfg(not(debug_assertions))]
        writeln!(&mut file, "\nError:\n\t{}", e)?;

        for cause in e.iter_causes() {
            eprintln!("Cause:\n\t{}", cause);
            #[cfg(not(debug_assertions))]
            writeln!(&mut file, "Cause:\n\t{}", cause)?;
        }

        // backtrace
        #[cfg(not(debug_assertions))]
        writeln!(&mut file, "\n{:?}", e)?;
    }

    Ok(())
}
