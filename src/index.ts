import "@babel/polyfill";

import Highcharts, {ChartObject, Gradient, SeriesObject} from "highcharts";
import Exporting from "highcharts/modules/exporting";
import "whatwg-fetch";

import "./style.scss";

Exporting(Highcharts);

const defaultHistorySize = 500;
// const maxHistorySize = 10000;

const defaultRefreshPeriod = 200;
const minRefreshPeriod = 50;

const defaultLineWidth = 1;
const minLineWidth = 1;

document.write(`
<!--<div id="tab-bar" class="tabs is-small">-->
<!--<ul>-->
<!--&lt;!&ndash;<li class="is-active"><a>File</a></li>&ndash;&gt;-->
<!--<li><a>File</a></li>-->
<!--<li><a>Edit</a></li>-->
<!--<li><a>View</a></li>-->
<!--</ul>-->
<!--</div>-->

<!--&lt;!&ndash; Main container &ndash;&gt;-->
<!--<nav id="top-bar" class="level">-->
<!--&lt;!&ndash; Left side &ndash;&gt;-->
<!--<div class="level-left">-->
<!--<div class="level-item">-->
<!--<div class="field has-addons">-->
<!--<p class="control">-->
<!--<input class="input" type="text" placeholder="Find a post">-->
<!--</p>-->
<!--<p class="control">-->
<!--<button class="button">-->
<!--Search-->
<!--</button>-->
<!--</p>-->
<!--</div>-->
<!--</div>-->
<!--<div class="level-item">-->
<!--<p class="subtitle is-5">-->
<!--<strong>123</strong> posts-->
<!--</p>-->
<!--</div>-->
<!--</div>-->

<!--&lt;!&ndash; Right side &ndash;&gt;-->
<!--<div class="level-right">-->
<!--<p class="level-item"><strong>All</strong></p>-->
<!--<p class="level-item"><a>Published</a></p>-->
<!--<p class="level-item"><a>Drafts</a></p>-->
<!--<p class="level-item"><a>Deleted</a></p>-->
<!--<p class="level-item"><a class="button is-success">New</a></p>-->
<!--</div>-->
<!--</nav>-->

<div id="chart"></div>
<section class="section">
    <div class="container">
        <!--<img src="chart.svg" alt="Chart" class="column">-->
        <div class="columns">
            <div class="column">
                <form id="dataForm">
                    <div class="field is-horizontal">
                        <div class="field-label is-small">
                            <label class="label">Refresh period (milliseconds)</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input name="refreshPeriod" class="input" type="text" placeholder="${defaultRefreshPeriod}">
                                </div>
                                <p class="help">Clamped to ${minRefreshPeriod} - inf</p>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-small">
                            <label class="label">History buffer size (frames)</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input name="historySize" class="input" type="text" placeholder="${defaultHistorySize}">
                                </div>
                                <p class="help">Clamped to 0 - inf</p>
                            </div>
                        </div>
                    </div>
                    <div class="field is-horizontal">
                        <div class="field-label is-small">
                            <label class="label">Line width</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input name="lineWidth" class="input" type="text" placeholder="${defaultLineWidth}">
                                </div>
                                <p class="help">Clamped to 1 - inf</p>
                            </div>
                        </div>
                    </div>

                    <!--<div class="control field">-->
                    <!--<input type="text" name="someinput" class="input" placeholder="Some input thing">-->
                    <!--</div>-->
                    <!--<div class="control field">-->
                    <!--<button type="submit" class="button">Some button</button>-->
                    <!--</div>-->
                </form>
            </div>
            <div class="column has-text-centered">
                <a id="hideAllSeries" class="button">Hide all data</a>
            </div>
        </div>
    </div>
</section>
`);

const dataForm = document.getElementById("dataForm")! as HTMLFormElement;
const historySizeElement = dataForm.elements.namedItem("historySize")! as HTMLTextAreaElement;
const refreshPeriodElement = dataForm.elements.namedItem("refreshPeriod")! as HTMLTextAreaElement;
const lineWidthElement = dataForm.elements.namedItem("lineWidth")! as HTMLTextAreaElement;
const chartElement = document.getElementById("chart")! as HTMLDivElement;
const hideAllSeriesElement = document.getElementById("hideAllSeries")! as HTMLButtonElement;

interface Print {
    kind: "Print",
    value: string,
}

type Message = Print;

function sendMessage(message: Message) {
    const _message: any = {};
    _message[message.kind] = { value: message.value };
    external.invoke(JSON.stringify(_message));
}

function print(message: string) {
    sendMessage({ kind: "Print", value: message });
}

// form.addEventListener("submit", (e) => {
//     e.preventDefault();
//     sendMessage({
//         kind: "Submit",
//         value: textBox.value,
//     });
// }, false);

const SPACING = 30;
const chart: ChartObject = Highcharts.chart(chartElement, {
    credits: { enabled: false },
    chart: {
        zoomType: "x",
        // height: "40%",
        spacing: [SPACING + 5, SPACING, 0, SPACING],
        animation: false,
    },
    title: {
        text: null,
    },
    xAxis: {
        title: { text: "Frame" },
    },
    yAxis: {
        title: null,
    },
    legend: {
        enabled: true,
    },
    plotOptions: {
        line: {
            // fillColor: {
            //     stops: [
            //         [0, Highcharts.getOptions().colors![0]],
            //         [1, (Highcharts.Color(Highcharts.getOptions().colors![0]) as Gradient)
            //             .setOpacity!(0)
            //             .get!("rgba"),
            //         ],
            //     ],
            // },
            // marker: {
            //     radius: defaultLineWidth + 1,
            // },
            lineWidth: defaultLineWidth,
            states: {
                hover: {
                    lineWidth: defaultLineWidth + 1,
                },
            },
            threshold: null,
        },
    },

    series: [
        {
            name: "Elapsed",
        },
        {
            name: "Desired Sleep",
        },
        {
            name: "Actual Sleep",
        },
        {
            name: "Oversleep",
        },
        {
            name: "Delta Time",
        },
        {
            name: "Run Frames",
        },
        {
            name: "Leftover Time",
        },
        {
            name: "Total",
        },
        {
            name: "Game",
        },
        {
            name: "Render",
        },
        {
            name: "GPU",
        },
    ],
});

let pointCount = 0;

function addPoint() {
    pointCount++;
}

hideAllSeriesElement.onclick = (e) => {
    chart.series.forEach((series) => {
        series.hide();
    });
};

let historySize = defaultHistorySize;

function clearData() {
    pointCount = 0;
    chart.series.forEach((series) => series.setData([], false));
    chart.redraw();
}

historySizeElement.addEventListener("input", (e) => {
    const n = parseInt(historySizeElement.value);
    if (isNaN(n) && historySize !== defaultHistorySize) {
        clearData();
        historySize = defaultHistorySize;
    } else {
        const newHist = Math.max(0, n);
        if (newHist !== historySize) {
            clearData();
            historySize = newHist;
        }
    }
});

function shouldShift(): boolean {
    return pointCount >= historySize;
}

let refreshPeriod = defaultRefreshPeriod;

refreshPeriodElement.addEventListener("input", (e) => {
    const n = parseInt(refreshPeriodElement.value);
    if (isNaN(n)) {
        refreshPeriod = defaultRefreshPeriod;
    } else {
        refreshPeriod = Math.max(minRefreshPeriod, n);
    }
});

lineWidthElement.addEventListener("input", (e) => {
    const n = parseInt(lineWidthElement.value);
    if (isNaN(n)) {
        chart.update({
            plotOptions: {
                line: {
                    lineWidth: defaultLineWidth,
                    // marker: {
                    //     lineWidth: defaultLineWidth + 1,
                    // },
                    states: {
                        hover: {
                            lineWidth: defaultLineWidth + 1,
                        },
                    },
                },
            },
        });
    } else {
        const newWidth = Math.max(minLineWidth, n);
        chart.update({
            plotOptions: {
                line: {
                    lineWidth: newWidth,
                    // marker: {
                    //     lineWidth: newWidth + 1,
                    // },
                    states: {
                        hover: {
                            lineWidth: newWidth + 1,
                        },
                    },
                },
            },
        });
    }
});

let lastDrawn = Date.now();

function drawChart() {
    if (Date.now() - lastDrawn > refreshPeriod) {
        // print(`${Date.now()}: Drawing`);
        chart.redraw();
        lastDrawn = Date.now();
    }
}

if (navigator.userAgent.match(/Trident\/7\./)) {
    (document.body as HTMLBodyElement).addEventListener("mousewheel", (event) => {
        const wheelEvent = event as WheelEventIE;
        wheelEvent.preventDefault();
        const wd = wheelEvent.wheelDelta;
        const csp = window.pageYOffset;
        window.scrollTo(0, csp - wd);
    });
}

const _global = global as any;
_global.chart = chart;
_global.addPoint = addPoint;
_global.clearData = clearData;
_global.shouldShift = shouldShift;
_global.drawChart = drawChart;
