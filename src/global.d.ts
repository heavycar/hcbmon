declare class External {
    public invoke(s: string): void;
}

declare interface WheelEventIE extends MouseEvent {
    wheelDelta: number;
    wheelDeltaX: number;
    wheelDeltaY: number;
}
