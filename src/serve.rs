use actix_web::{
    middleware::csrf,
    server::HttpServer,
    App,
    HttpRequest,
    HttpResponse,
    Result as AWResult,
};
use include_dir::Dir;
use mime_guess::guess_mime_type;
use std::{io, path::PathBuf};

const SITE_DATA: Dir = include_dir!("./dist");

fn index(req: &HttpRequest) -> AWResult<HttpResponse> {
    let mut path: PathBuf = req.match_info().query("tail")?;
    if path.to_string_lossy().is_empty() {
        path.push("index.html");
    }
    Ok(match SITE_DATA.get_file(&path) {
        Some(file) => HttpResponse::Ok()
            .content_type(guess_mime_type(&path).as_ref())
            .body(file.contents()),
        None => HttpResponse::NotFound().body("404 Not Found"),
    })
}

pub fn build() -> io::Result<HttpServer<App<()>, impl Fn() -> App<()> + Send + Clone + 'static>> {
    HttpServer::new(|| {
        App::new()
            .middleware(csrf::CsrfFilter::new().allowed_origin("127.0.0.1"))
            .resource("/{tail:.*}", |r| r.f(index))
    })
    .bind("127.0.0.1:0")
}
